#!/usr/bin/env python

import os
import requests

BITBUCKET_USER = os.environ.get("BITBUCKET_USERNAME")
BITBUCKET_KEY = os.environ.get("BITBUCKET_PASSWORD")
BITBUCKET_REPO_OWNER = os.environ.get("BITBUCKET_REPO_OWNER")
BITBUCKET_REPO_SLUG = os.environ.get("BITBUCKET_REPO_SLUG")
BITBUCKET_BRANCH = os.environ.get("BITBUCKET_BRANCH")
BITBUCKET_COMMIT = os.environ.get("BITBUCKET_COMMIT")


request_url = "https://api.bitbucket.org/2.0/repositories/%s/%s/pipelines/?sort=-created_on" % (BITBUCKET_REPO_OWNER, BITBUCKET_REPO_SLUG)
r = requests.get(request_url, auth=(BITBUCKET_USER, BITBUCKET_KEY))
result = r.json()
uuids = []
for value in result["values"]:
 target = value.get("target")
 if target.get("ref_name") == BITBUCKET_BRANCH:
  type = value.get("state").get("type")
  if type == "pipeline_state_in_progress" or type == "pipeline_state_pending":
   if target.get("commit").get("type") == "commit":
    if target.get("commit").get("hash") != BITBUCKET_COMMIT:
     uuids.append(value.get("uuid"))
for uuid in uuids:
 request_url = "https://api.bitbucket.org/2.0/repositories/%s/%s/pipelines/%s/stopPipeline" % (BITBUCKET_REPO_OWNER, BITBUCKET_REPO_SLUG, uuid)
 response = requests.post(request_url, auth=(BITBUCKET_USER, BITBUCKET_KEY))
